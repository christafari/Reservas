
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 07/28/2015 18:29:59
-- Generated from EDMX file: C:\Users\ing_christian\documents\visual studio 2013\Projects\WcfServiceBuses\WcfServiceBuses\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Reservas];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ViajeReserva]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ReservaSet] DROP CONSTRAINT [FK_ViajeReserva];
GO
IF OBJECT_ID(N'[dbo].[FK_UsuarioReserva]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ReservaSet] DROP CONSTRAINT [FK_UsuarioReserva];
GO
IF OBJECT_ID(N'[dbo].[FK_BusViaje]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ViajeSet] DROP CONSTRAINT [FK_BusViaje];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[BusSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BusSet];
GO
IF OBJECT_ID(N'[dbo].[ReservaSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ReservaSet];
GO
IF OBJECT_ID(N'[dbo].[ViajeSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ViajeSet];
GO
IF OBJECT_ID(N'[dbo].[UsuarioSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UsuarioSet];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'BusSet'
CREATE TABLE [dbo].[BusSet] (
    [Id_Bus] int IDENTITY(1,1) NOT NULL,
    [Capacidad] nvarchar(max)  NOT NULL,
    [Tipo] nvarchar(max)  NOT NULL,
    [Horario] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'ReservaSet'
CREATE TABLE [dbo].[ReservaSet] (
    [Id_Reserva] int IDENTITY(1,1) NOT NULL,
    [Puesto] nvarchar(max)  NOT NULL,
    [Ubicacion_p] nvarchar(max)  NOT NULL,
    [ViajeId_Reserva] int  NOT NULL,
    [UsuarioId_Usuario] int  NOT NULL
);
GO

-- Creating table 'ViajeSet'
CREATE TABLE [dbo].[ViajeSet] (
    [Id_Reserva] int IDENTITY(1,1) NOT NULL,
    [Origen] nvarchar(max)  NOT NULL,
    [Destino] nvarchar(max)  NOT NULL,
    [Fecha] nvarchar(max)  NOT NULL,
    [Valor] nvarchar(max)  NOT NULL,
    [Bus_Id_Bus] int  NOT NULL
);
GO

-- Creating table 'UsuarioSet'
CREATE TABLE [dbo].[UsuarioSet] (
    [Id_Usuario] int IDENTITY(1,1) NOT NULL,
    [Cedula] nvarchar(max)  NOT NULL,
    [Nombre] nvarchar(max)  NOT NULL,
    [Fecha_Nacimiento] nvarchar(max)  NOT NULL,
    [Numero_Celular] nvarchar(max)  NOT NULL,
    [Correo] nvarchar(max)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id_Bus] in table 'BusSet'
ALTER TABLE [dbo].[BusSet]
ADD CONSTRAINT [PK_BusSet]
    PRIMARY KEY CLUSTERED ([Id_Bus] ASC);
GO

-- Creating primary key on [Id_Reserva] in table 'ReservaSet'
ALTER TABLE [dbo].[ReservaSet]
ADD CONSTRAINT [PK_ReservaSet]
    PRIMARY KEY CLUSTERED ([Id_Reserva] ASC);
GO

-- Creating primary key on [Id_Reserva] in table 'ViajeSet'
ALTER TABLE [dbo].[ViajeSet]
ADD CONSTRAINT [PK_ViajeSet]
    PRIMARY KEY CLUSTERED ([Id_Reserva] ASC);
GO

-- Creating primary key on [Id_Usuario] in table 'UsuarioSet'
ALTER TABLE [dbo].[UsuarioSet]
ADD CONSTRAINT [PK_UsuarioSet]
    PRIMARY KEY CLUSTERED ([Id_Usuario] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ViajeId_Reserva] in table 'ReservaSet'
ALTER TABLE [dbo].[ReservaSet]
ADD CONSTRAINT [FK_ViajeReserva]
    FOREIGN KEY ([ViajeId_Reserva])
    REFERENCES [dbo].[ViajeSet]
        ([Id_Reserva])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ViajeReserva'
CREATE INDEX [IX_FK_ViajeReserva]
ON [dbo].[ReservaSet]
    ([ViajeId_Reserva]);
GO

-- Creating foreign key on [UsuarioId_Usuario] in table 'ReservaSet'
ALTER TABLE [dbo].[ReservaSet]
ADD CONSTRAINT [FK_UsuarioReserva]
    FOREIGN KEY ([UsuarioId_Usuario])
    REFERENCES [dbo].[UsuarioSet]
        ([Id_Usuario])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UsuarioReserva'
CREATE INDEX [IX_FK_UsuarioReserva]
ON [dbo].[ReservaSet]
    ([UsuarioId_Usuario]);
GO

-- Creating foreign key on [Bus_Id_Bus] in table 'ViajeSet'
ALTER TABLE [dbo].[ViajeSet]
ADD CONSTRAINT [FK_BusViaje]
    FOREIGN KEY ([Bus_Id_Bus])
    REFERENCES [dbo].[BusSet]
        ([Id_Bus])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BusViaje'
CREATE INDEX [IX_FK_BusViaje]
ON [dbo].[ViajeSet]
    ([Bus_Id_Bus]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------