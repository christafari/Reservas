﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Web.Script.Serialization;

namespace WcfServiceBuses
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class Service1 : IService1
    {
      
 
        
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }
        public void eliminarReserva()
        {


        } 
        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }


        Model1Container db = new Model1Container();
       
        public String retornarbuses(string co,string cd){

            JavaScriptSerializer serializador = new JavaScriptSerializer();
            String JSON = string.Empty;
            

            var bu = from viaje in db.ViajeSet
                       where  viaje.Origen==co && viaje.Destino == cd 
                       select new { id_bus = viaje.Bus };

          List<Bus> Disponibles = new List<Bus>();
            foreach (var usuario in bu)
            {
                var bus = from buses in db.BusSet
                          where buses.Id_Bus == usuario.id_bus.Id_Bus
                          select new
                          {
                              Id_bus = buses.Id_Bus,
                              Tipo = buses.Tipo,
                              capacidad = buses.Capacidad
                          }                          
                         ;
                foreach (var datos in bus)
                {
                    Bus a = new Bus();
                    a.Id_Bus = datos.Id_bus;
                    a.Tipo = datos.Tipo;
                    a.Capacidad = datos.capacidad;
                    Disponibles.Add(a);
                }


              
            }
            JSON = serializador.Serialize(Disponibles);
                return JSON;
        }



        public String consultar_reservas(int id_usuario){

            JavaScriptSerializer serializador = new JavaScriptSerializer();
            String JSON = string.Empty;
            var registro = from reserva   in db.ReservaSet
                           join viaje in db.ViajeSet
                           on reserva.ViajeId_Reserva equals viaje.Id_Reserva
                           where reserva.UsuarioId_Usuario == id_usuario
                           select new {
                           origen =viaje.Origen,
                           destino = viaje.Destino,
                           id_reserva = reserva.Id_Reserva,
                           puesto = reserva.Puesto
                           };

            List<Object> reservas = new List<Object>();

            foreach (var reserva in registro)
              {
                  Reserva a = new Reserva();
                    a.Id_Reserva=reserva.id_reserva;
                    a.Puesto = reserva.puesto;
                    reservas.Add(a);
                     Viaje b =new Viaje();
                     b.Origen=reserva.origen;
                      b.Destino=reserva.destino;
                reservas.Add(b);

                }
                JSON = serializador.Serialize(reservas);
                return JSON;
            
            
        }
    }
}
