//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WcfServiceBuses
{
    using System;
    using System.Collections.Generic;
    
    public partial class Bus
    {
        public Bus()
        {
            this.Viaje = new HashSet<Viaje>();
        }
    
        public int Id_Bus { get; set; }
        public string Capacidad { get; set; }
        public string Tipo { get; set; }
        public string Horario { get; set; }
    
        public virtual ICollection<Viaje> Viaje { get; set; }
    }
}
